import java.util.*;

public class GraphTask {
         
   public static void main (String[] args) {
	  GraphTask a = new GraphTask();
	  StringBuffer sb = new StringBuffer();
	  for (String s: args) {		  
		  s = s.replaceAll("\\t", "\\s");		  		  		 
		  sb.append(s);
	  }
	  String graphString = sb.toString();
	  Graph g = a.graphFromString(graphString);
	  System.out.println("Graph: " + graphString);
      System.out.println(g);
      g.printAdjMatrix();
      g.printDistMatrix();      
      g.printPredecessorMatrix();
      g.printLongestPath();		  
   }

   /**
    * Runs 8 different test cases for various different graphs. 
    */
   public void run() {

		  // Test case 1. Distance between specific pair (v2-v4) is longest
		  Graph graph1 = graphFromString("V4-V5;V1-V6;V2-V6;V3-V4;V3-V6;V5-V1;V5-V6;V1-V3");
	      System.out.println("Test case 1. Distance between specific pair (v2-v4) is longest");
	      System.out.println(graph1);
	      graph1.printAdjMatrix();
	      graph1.printDistMatrix();      
	      graph1.printPredecessorMatrix();
	      graph1.printLongestPath();
	      
	      
	      
		  // Test case 2. Distance between several pairs is longest
	      graph1 = graphFromString("V4-V5;V1-V6;V2-V6;V3-V4;V3-V6;V5-V1;V5-V6;V1-V3;V2-V3");
	      System.out.println("Test case 2. Distance between several pairs is longest");
	      System.out.println(graph1);
	      graph1.printAdjMatrix();
	      graph1.printDistMatrix();      
	      graph1.printLongestPath();
	      
	      
	      
	      // Test case 3. Empty graph
	      Graph graph2 = new Graph ("Graph 2");
	      System.out.println("Test case 3. Empty graph");
	      System.out.println(graph2);
	      graph2.printAdjMatrix();
	      graph2.printDistMatrix();      
	      graph2.printLongestPath();      

	      
	      
	      // Test case 4. Graph with single vertex
	      Vertex w6 = graph2.createVertex("w6");
	      System.out.println("Test case 4. Graph with single vertex");
	      System.out.println(graph2);
	      graph2.printAdjMatrix();
	      graph2.printDistMatrix();      
	      graph2.printLongestPath();
	      
	      
	      
	      // Test case 5. Graph with linear chain of vertices
	      graph2 = graphFromString("W1-W2;W2-W3;W3-W4;W4-W5;W5-W6");      
	      System.out.println("Test case 5. Graph with linear chain of vertices");
	      System.out.println(graph2);
	      graph2.printAdjMatrix();
	      graph2.printDistMatrix();      
	      graph2.printLongestPath();

	      
	      
	      // Test case 6. Graph with one single loop
	      graph2 = graphFromString("W1-W2;W2-W3;W3-W4;W4-W5;W5-W6;W6-W1");
	      System.out.println("Test case 5. Graph with one single loop");
	      System.out.println(graph2);
	      graph2.printAdjMatrix();
	      graph2.printDistMatrix();      
	      graph2.printLongestPath();
	      
	      
	      
	      // Test case 7. Graph with two components
	      graph1 = graphFromString("V4-V5;V1-V6;V2-V6;V3-V4;V3-V6;V5-V1;V5-V6;V1-V3;Q1-Q2;Q2-Q3;Q2-Q4;Q1-Q5");                 
	      
	      System.out.println("Test case 7. Graph with two components");
	      System.out.println(graph1);
	      graph1.printAdjMatrix();
	      graph1.printDistMatrix();      
	      graph1.printLongestPath();
	      
	      
	      
	      // Test case 8. Directed graph
	      Graph graph4 = new Graph("Testcase8");
	      Vertex r6 = graph4.createVertex("r6");
	      Vertex r5 = graph4.createVertex("r5");
	      Vertex r4 = graph4.createVertex("r4");
	      Vertex r3 = graph4.createVertex("r3");
	      Vertex r2 = graph4.createVertex("r2");
	      Vertex r1 = graph4.createVertex("r1");
	      
	      graph4.createArc("ar1_r3", r1, r3);
	      graph4.createArc("ar1_r6", r1, r6);
	      graph4.createArc("ar2_r3", r2, r3);
	      graph4.createArc("ar3_r4", r3, r4);
	      graph4.createArc("ar4_r5", r4, r5);
	      graph4.createArc("ar6_r2", r6, r2);
	      graph4.createArc("ar6_r5", r6, r5);
	      
	      graph4.printAdjMatrix();
	      graph4.printDistMatrix();      
	      graph4.printLongestPath();	      
   }
   
   

   /**
    * Creates a graph from string. String should contain arcs split by ";" symbols.
    * Every arc should contain two vertices joined by symbol "-". For example, string:
    * "A-B;B-C;C-D" would return graph with three vertices (A, B, C), with three arcs
    * between A-B, B-C and C-D
    * @param string describing graph
    * @return created graph.
    */
   public Graph graphFromString(String input) {
	   
	   if (input == null || input.length() == 0) throw new RuntimeException("Empty string as input.");
	   
	   List<String> listVertexString = new ArrayList<String>();
	   List<Vertex> listVertex = new ArrayList<Vertex>();
	   String[] stringArray = input.split(";");
	   
	   Graph graph = new Graph("graph from string");
	   
	   for (int i=0; i<stringArray.length;i++) {
		   String s = stringArray[stringArray.length - 1 - i];
		   		   
		   if (s == null || s.length() == 0) continue;
		   		  		   
		   String[] stringPair = s.split("-");
		   if (stringPair.length > 2) throw new RuntimeException("Arc contains more than two vertices: " + s);
		   if (stringPair.length == 1) throw new RuntimeException("Arc contains only one vertex: " + s);
		   
		   String startVertex = stringPair[0].trim();
		   String endVertex = stringPair[1].trim();
		   
		   if (startVertex.contains(" ")) throw new RuntimeException("Vertex contains spaces: " + startVertex);
		   if (endVertex.contains(" ")) throw new RuntimeException("Vertex contains spaces: " + endVertex);		   
		   if (startVertex == null || startVertex.length() == 0) throw new RuntimeException("Empty string for start vertex.");
		   if (endVertex == null || endVertex.length() == 0) throw new RuntimeException("Empty string for end vertex.");
		   		   
		   if (!listVertexString.contains(startVertex)) {
			   listVertexString.add(startVertex);
			   listVertex.add(graph.createVertex(startVertex));
		   }
		   
		   if (!listVertexString.contains(endVertex)) {
			   listVertexString.add(endVertex);
			   listVertex.add(graph.createVertex(endVertex));
		   }
		   		   
		   graph.createArc(
				   startVertex + "_" + endVertex,
				   listVertex.get(listVertexString.indexOf(startVertex)),
				   listVertex.get(listVertexString.indexOf(endVertex))
				   );
		   
		   graph.createArc(
				   endVertex + "_" + startVertex,
				   listVertex.get(listVertexString.indexOf(endVertex)),
				   listVertex.get(listVertexString.indexOf(startVertex))
				   );		   
	   }	   
	   return graph;
   }

   
   class Vertex {
      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

   } 


   
   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private int[][] distMatrix;
      private int[][] predecessorMatrix;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      
      
      /**
       * Create a vertex.
       * @param vertex name string
       * @return created vertex
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      
      
      /**
       * Create a arc connecting two vertices.
       * @param arc name string
       * @param vertex where arc starts
       * @param vertex where arc ends
       * @return created arc
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      
      
      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      
      
      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      
      
      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      
      
      /**
       * Prints adjacency matrix to console. 
       */ 
      public void printAdjMatrix() {
          int[][] adjMatrix = this.createAdjMatrix();
          System.out.println("adjacency matrix:");
          printMatrix(adjMatrix);
          System.out.println("");
      }
      
      
      
      /**
       * Prints distance matrix to console. 
       */
      public void printDistMatrix() {
          this.createDistMatrix();
          System.out.println("distance matrix:");
          printMatrix(distMatrix);
          System.out.println("");
      }
      
      
      
      /**
       * Prints distance matrix to console. 
       */
      public void printPredecessorMatrix() {
          this.createDistMatrix();
          System.out.println("predecessors matrix:");
          printMatrix(predecessorMatrix);
          System.out.println("");
      }
      
      
      
      /**
       * Prints matrix to console. 
       */
      private void printMatrix(int[][] matrix) {
          for (int[] tmp : matrix) {
        	  System.out.println(Arrays.toString(tmp));
          }
      }
      
      
      
      /**
       * Prints shortest path to console between vertices separated
       * by longest path. 
       */
      public void printLongestPath() {
    	  String nl = System.getProperty ("line.separator");
          StringBuffer sb = new StringBuffer("");
          
          List<Vertex> listVertex = this.getVertexList();
          
    	  List<int[]> longestPaths = this.findLongestPathPairs();
    	  if (longestPaths.size() == 0) {
    		  System.out.println("longest path(s):");
              System.out.println("-");
              System.out.println("");
              return;
    	  }    	  
    	  
          for (int[] tmp: longestPaths) {        	  
        	  
        	  sb.append(listVertex.get(tmp[0]).toString());
        	  sb.append(" --> ");
        	  sb.append(listVertex.get(tmp[1]).toString());
        	  sb.append(" : (");
        	  
        	  List<Integer> path = this.findShortestPath(tmp[0], tmp[1]);
        	  for (int i: path) {
        		  sb.append(listVertex.get(i).toString());
        		  sb.append(" -> ");
        	  }
        	  sb.replace(sb.length() - 4, sb.length(), ")");
        	  sb.append(nl);
          }
          
          System.out.println("longest path(s):");
          System.out.println(sb.toString());    
          System.out.println("");
      }
            
 
      
      /**
       * Calculates shortest matrix with values corresponding to shortest
       * distance between two vertex using Floyd-Warshall algorithm. Finds
       * all shortest paths among all pairs of vertices. 
       * @return matrix of distance
       */     
      private void createDistMatrix() {
       	  // Create initial distance matrix 
    	  int[][] distMatrix = this.createAdjMatrix();
    	  for (int i=0; i < distMatrix.length; i++) {
              for (int j=0; j < distMatrix.length; j++) {
        		  if (distMatrix[i][j] == 0 && i!=j) {    			  
        			  distMatrix[i][j] = Integer.MAX_VALUE;        			 
        		  }
              }
    	  }
         	      	      	      	 
    	  // Temporary matrix
    	  int[][] predecessorMatrix = new int[distMatrix.length][distMatrix.length];
          for (int i=0; i < distMatrix.length; i++) {
              for (int j=0; j < distMatrix.length; j++) {
                  if (distMatrix[i][j] != 0 && distMatrix[i][j] != Integer.MAX_VALUE) {
                      predecessorMatrix[i][j] = i;
                  } else {
                      predecessorMatrix[i][j] = -1;
                  }
              }
          }

          // Calculate distances
          for (int k=0; k < distMatrix.length; k++) {              
              for (int i=0; i < distMatrix.length; i++) {
                  for (int j=0; j < distMatrix.length; j++) {
                      // If i->j or k->j are not connected, then continue with next
                	  if (distMatrix[i][k] == Integer.MAX_VALUE || distMatrix[k][j] == Integer.MAX_VALUE) {
                          continue;                  
                      }
                      
                      // If path i->j is longer than i->k->j, then replace values 
                      if (distMatrix[i][j] > distMatrix[i][k] + distMatrix[k][j]) {
                          distMatrix[i][j] = distMatrix[i][k] + distMatrix[k][j];
                          predecessorMatrix[i][j] = predecessorMatrix[k][j];
                      }
                  }
              }
          }          
          
          // replace infinities with zeros
          for (int i=0; i < distMatrix.length; i++) {
              for (int j=0; j < distMatrix.length; j++) {
                 if (distMatrix[i][j] == Integer.MAX_VALUE) distMatrix[i][j] = 0; 	  
              }
          }
          this.distMatrix = distMatrix;
          this.predecessorMatrix = predecessorMatrix;
      }
      
      
      
      /**
       * Finds pairs of vertices that have longest path between them
       * and returns indeces of vertices. Pairs are found using
       * Floyd-Warshall algorithm.
       * @return array of integer pairs
       */
      public List<int[]> findLongestPathPairs() {
    	      	      	  
    	  // Find longest path length
    	  this.createDistMatrix();
    	  List<int[]> vertexPairs = new ArrayList<int[]>();
    	  int longestPath = Integer.MIN_VALUE;
    	  for (int i=0; i < distMatrix.length; i++) {
    		  for (int j=0; j < distMatrix.length; j++) {
    			  if (i==j) continue;
    			  
    			  if (distMatrix[i][j] > longestPath) {
    				  longestPath = distMatrix[i][j];
    				  vertexPairs = new ArrayList<int[]>();
    				  vertexPairs.add(new int[]{i, j});
    			  } else if (distMatrix[i][j] == longestPath) {     				  
    				  // Skippes B-to-A if A-to-B is already in the list and has identical length
    				  if (j < i && distMatrix[i][j] == distMatrix[j][i]) {
    					  continue;
    				  } else {
    					  vertexPairs.add(new int[]{i, j});    					  
    				  }
    			  }
    		  }
    	  }
    	      	 
    	  return vertexPairs;
      }
      
      
      
	  /**
	   * Finds shortest path between two given vertices. A
	   * @param start vertex
	   * @param end vertex 
	   * @return array of integers
	   */
      public List<Integer> findShortestPath(int vertexStart, int vertexEnd) {
    	  List<Integer> connections = new ArrayList<Integer>();
    	  if (vertexStart == vertexEnd) {
    		  connections.add(vertexStart);
    		  return connections;
    	  } else if (predecessorMatrix[vertexStart][vertexEnd] == -1) {
    		  return new ArrayList<Integer>();
    	  } else {
    		  connections = findShortestPath(vertexStart, predecessorMatrix[vertexStart][vertexEnd]);
    		  connections.add(vertexEnd);
    	  }
    	  
    	  return connections;
      }
      
      
      
      /**
       * Finds the list of vertices.
       * @return List of vertices
       */  
      public List<Vertex> getVertexList() {    	  
    	  List<Vertex> listVertex = new ArrayList<Vertex>();          
    	  Vertex v = first;    	  
          while (v != null) {
        	  listVertex.add(v);
        	  v = v.next;             
        	  }
          return listVertex;
      }
   }
} 